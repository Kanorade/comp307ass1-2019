Different ways to compile and run these programs:

Run the executable jars:

Provided are the executable jars NearestNeighbour.jar and DecisionTreeLearning.jar
you can run these by typing in command line, for example:

java -jar NearestNeighbour.jar iris-training.txt iris-test.txt

or

java -jar DecisionTreeLearning.jar hepatitis-training.dat hepatitis-test.dat


Make sure the data files are in the same directory as the executable jars or write
the file path.

-----------------------------------------------------------

Import to eclipse:

-Create new project and import comp307ass1.jar

I'm assuming you're familiar with eclipse here so I won't go into further detail.

When running either DecisionTreeLearning.java or NearestNeighbour.java you'll need
to add arguments to the run configuration.

For example, be sure to have the data files in the project directory. Right click
the java file you want to run -> Run as... -> Run Configurations -> Arguments and
under Program Arguments type in the file names like
"iris-training.txt iris-test.txt" or the file path to the data files.