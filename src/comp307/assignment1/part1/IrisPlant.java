package comp307.assignment1.part1;

import java.util.ArrayList;

/**
 * Simple Iris plant class. This is purely for testing and for assignment use.
 * So I'm just keeping it simple and having the fields publicly accessible;
 * with minimal restrictions.
 *
 * @author Mikey Kane
 *
 */
public class IrisPlant {
	public ArrayList<Float> values;
	public String classifier;

	public IrisPlant(float sepalLength, float sepalWidth, float petalLength, float petalWidth, String classifier) {
		values = new ArrayList<Float>();
		values.add(sepalLength);
		values.add(sepalWidth);
		values.add(petalLength);
		values.add(petalWidth);

		this.classifier = classifier;
	}

	@Override
	public String toString() {
		return "IrisPlant [values=" + values + ", classifier=" + classifier + "]";
	}
}
