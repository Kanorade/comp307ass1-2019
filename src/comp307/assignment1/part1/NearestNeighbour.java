package comp307.assignment1.part1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;

/**
 * Nearest Neighbour algorithm to classify plants in a test set based on instances of plants
 * in a training set.
 *
 * @author Mikey Kane
 *
 */
public class NearestNeighbour {
	Set<IrisPlant> trainingSet;
	Set<IrisPlant> testSet;

	public NearestNeighbour(String trainingFileName, String testFileName) {
		// Create 2 sets from the files given
		trainingSet = new HashSet<IrisPlant>();		// If you care about order, use LinkedHashSet
		testSet = new HashSet<IrisPlant>();

		System.out.println("Reading training and test files...");

		// Build sets to work on
		buildSetFromFile(trainingSet, trainingFileName);
		buildSetFromFile(testSet, testFileName);

		System.out.println("Training Set, " + trainingSet.size() + " instances");
		System.out.println("Test Set, " + testSet.size() + " instances\n");
	}

	/**
	 * Where most of the heavy lifting happens. Will classify each instance on test set
	 * according to the training set.
	 * @param kValue
	 */
	public void classifyInstanciesOnTestSet(int kValue) {
		System.out.println("Classifying test instances with kValue = " + kValue + ":");
		System.out.println();

		int successCounter = 0;
		ArrayList<Float> plantRanges = findRanges(trainingSet);

		// Go through each plant in the testSet and classify them
		for (IrisPlant testPlant : testSet) {
			System.out.print("Iris Plant Values: " + testPlant.values + ", Name:\t" + testPlant.classifier + "\n\t\t\t    Classified as ->\t");

			// Create a Map of training plants and their distances from testPlant
			HashMap<Float, IrisPlant> trainPlantMap = new HashMap<Float, IrisPlant>();
			PriorityQueue<Float> orderedDists = new PriorityQueue<>();

			// Compare testPlant with each training plant in training set
			for (IrisPlant trainPlant : trainingSet) {
				float distance = calculateDistance(testPlant, trainPlant, plantRanges);

				/* The danger to a HashMap is not all floats are unique. For the scope
				 * of the assignment, this should be ok for now.
				 *
				 * In the future the priority queue and the Map should be combined to just
				 * one priority queue, combining distance and plants into one private class.
				 */

				// Store distance information
				orderedDists.add(distance);
				trainPlantMap.put(distance, trainPlant);

			}

			// Use distance info to get classification
			String winningClassification = determineClassification(orderedDists, trainPlantMap, kValue);
			System.out.println(winningClassification);
			System.out.println();

			// Check if classification is a matches the real life answer
			if (testPlant.classifier.equals(winningClassification)) {
				successCounter++;
			}
		}
		System.out.println("----------------------------------------");
		System.out.print("With k = " + kValue + ", ");
		System.out.println("Accuracy: " + (((float)successCounter/testSet.size())*100) + "%");
	}

	/**
	 * Return classification from the given information.
	 * @param orderedDists A priority queue starting from shortest distance.
	 * @param trainPlantMap A Map of plants with their distances are the key.
	 * @param kValue How many plants nearby worth taking into account.
	 * @return plant classification
	 */
	private String determineClassification(PriorityQueue<Float> orderedDists, HashMap<Float, IrisPlant> trainPlantMap,
			int kValue) {
		// Get smallest distances
		HashMap<String, Integer> tally = new HashMap<>();
		for (int i = 0; i < kValue; i++) {
			float nearest = orderedDists.poll();
			String classification = trainPlantMap.get(nearest).classifier;
			if (tally.containsKey(classification)) {
				tally.put(classification, tally.get(classification)+1);
			} else {
				tally.put(classification, 1);
			}
		}

		String winningClassification = "";
		for (Map.Entry<String, Integer> entry : tally.entrySet()) {
			if (winningClassification.equals("")) {
				winningClassification = entry.getKey();
			} else {
				if (entry.getValue() > tally.get(winningClassification)) {
					winningClassification = entry.getKey();
				}
			}
		}
		return winningClassification;
	}

	/**
	 * Figure out the mathematical distance between 2 plants
	 * @param testPlant Plant from test set
	 * @param trainPlant Plant from training set
	 * @param plantRanges Ranges from the training set
	 * @return Distance between plants
	 */
	private float calculateDistance(IrisPlant testPlant, IrisPlant trainPlant,
			ArrayList<Float> plantRanges) {
		// Using Euclidean distance

		// Implement the part of Euclidean distance where the normalised ranges are added.
		// ie. everything but the square roots.
		float beforeSqrt = 0;
		for (int i = 0; i < testPlant.values.size(); i++) {
			// (p1 - p2) ^ 2
			float ans = (float) Math.pow(testPlant.values.get(i) - trainPlant.values.get(i), 2);
			// ans / R^2
			ans = ans / (float) Math.pow(plantRanges.get(i), 2);

			beforeSqrt += ans;
		}

		return (float) Math.sqrt(beforeSqrt);
	}

	/**
	 * Return a list of ranges for each value of the plant in the plant Set
	 * @param plantSet
	 * @return Value ranges for the plant set.
	 */
	private ArrayList<Float> findRanges(Set<IrisPlant> plantSet) {
		ArrayList<Float> minValues = new ArrayList<>();
		ArrayList<Float> maxValues = new ArrayList<>();

		for (IrisPlant plant : plantSet) {
			if (minValues.isEmpty() || maxValues.isEmpty()) {
				minValues.addAll(plant.values);
				maxValues.addAll(plant.values);
			}
			else {
				for (int i = 0; i < plant.values.size(); i++) {
					if (plant.values.get(i) < minValues.get(i)) {
						minValues.set(i, plant.values.get(i));
					}
					if (plant.values.get(i) > maxValues.get(i)) {
						maxValues.set(i, plant.values.get(i));
					}
				}
			}
		}

		//System.out.println("Min values: " + minValues);
		//System.out.println("Max values: " + maxValues);

		ArrayList<Float> ranges = new ArrayList<>();
		for(int i = 0; i < minValues.size(); i++) {
			ranges.add(maxValues.get(i) - minValues.get(i));
		}
		//System.out.println("Ranges: " + ranges);
		return ranges;
	}

	/**
	 * Reads plant data in files and stores them into sets.
	 * @param set To store the plant data
	 * @param fileName name of the file to get the data from
	 */
	private void buildSetFromFile(Set<IrisPlant> set, String fileName) {
		File trainingFile = new File(fileName);
		try (BufferedReader br = new BufferedReader(new FileReader(trainingFile))){
			String line;
			while ((line = br.readLine()) != null) {
				if (line.trim().isEmpty()) {
					break;
				}
				String[] tokens = line.split("  ");
				// Create an Irisplant from each line
				IrisPlant ip = new IrisPlant(
						Float.parseFloat(tokens[0]),
						Float.parseFloat(tokens[1]),
						Float.parseFloat(tokens[2]),
						Float.parseFloat(tokens[3]),
						tokens[4]
						);

				set.add(ip);
			}
		} catch (FileNotFoundException e) {
			System.out.println("Couldn't find the file(s) specified.");
			//e.printStackTrace();
			System.exit(0);
		} catch (IOException e1) {
			System.out.println("Something went wrong while reading the files: IOException");
			e1.printStackTrace();
			System.exit(0);
		}
	}

	public static void main(String[] args) {
		if (args.length == 2) {
			NearestNeighbour nn = new NearestNeighbour(args[0], args[1]);
			nn.classifyInstanciesOnTestSet(1);
        } else {
            System.out.println("Require 2 arguments.");
            System.out.println("Usage: NearestNeighbour trainingfile.txt testfile.txt");
        }
	}

}
