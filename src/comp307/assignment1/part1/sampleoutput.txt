Reading training data from file part2/hepatitis-training.dat:

Number of categories: 2
categoryNames = [live, die]
Number of attributes: 16
attributeNames = [AGE, FEMALE, STEROID, ANTIVIRALS, FATIGUE, MALAISE, ANOREXIA, BIGLIVER, FIRMLIVER, SPLEENPALPABLE, SPIDERS, ASCITES, VARICES, BILIRUBIN, SGOT, HISTOLOGY]
Number of Instances: 110

Generating decision tree:
	FEMALE = True:
		Class live, prob=1.00
	FEMALE = False:
		FATIGUE = True:
			SPLEENPALPABLE = True:
				BIGLIVER = True:
					ANTIVIRALS = True:
						SGOT = True:
							Class live, prob=1.00
						SGOT = False:
							VARICES = True:
								SPIDERS = True:
									FIRMLIVER = True:
										Class live, prob=1.00
									FIRMLIVER = False:
										Class die, prob=1.00
								SPIDERS = False:
									Class live, prob=1.00
							VARICES = False:
								Class die, prob=1.00
					ANTIVIRALS = False:
						Class live, prob=1.00
				BIGLIVER = False:
					Class live, prob=1.00
			SPLEENPALPABLE = False:
				Class live, prob=1.00
		FATIGUE = False:
			ASCITES = True:
				SPIDERS = True:
					VARICES = True:
						SPLEENPALPABLE = True:
							BIGLIVER = True:
								ANOREXIA = True:
									SGOT = True:
										Class live, prob=1.00
									SGOT = False:
										HISTOLOGY = True:
											Class live, prob=1.00
										HISTOLOGY = False:
											AGE = True:
												Class live, prob=1.00
											AGE = False:
												MALAISE = True:
													Class live, prob=1.00
												MALAISE = False:
													FIRMLIVER = True:
														Class live, prob=1.00
													FIRMLIVER = False:
														Class die, prob=1.00
								ANOREXIA = False:
									Class live, prob=1.00
							BIGLIVER = False:
								Class live, prob=1.00
						SPLEENPALPABLE = False:
							Class live, prob=1.00
					VARICES = False:
						Class die, prob=1.00
				SPIDERS = False:
					SPLEENPALPABLE = True:
						AGE = True:
							Class die, prob=1.00
						AGE = False:
							VARICES = True:
								ANOREXIA = True:
									MALAISE = True:
										ANTIVIRALS = True:
											STEROID = True:
												Class die, prob=1.00
											STEROID = False:
												Class live, prob=1.00
										ANTIVIRALS = False:
											Class live, prob=1.00
									MALAISE = False:
										ANTIVIRALS = True:
											SGOT = True:
												Class die, prob=1.00
											SGOT = False:
												Class live, prob=1.00
										ANTIVIRALS = False:
											Class die, prob=1.00
								ANOREXIA = False:
									Class live, prob=1.00
							VARICES = False:
								Class live, prob=1.00
					SPLEENPALPABLE = False:
						BIGLIVER = True:
							AGE = True:
								VARICES = True:
									Class die, prob=1.00
								VARICES = False:
									Class live, prob=1.00
							AGE = False:
								Class die, prob=1.00
						BIGLIVER = False:
							Class die, prob=1.00
			ASCITES = False:
				BIGLIVER = True:
					BILIRUBIN = True:
						AGE = True:
							Class die, prob=1.00
						AGE = False:
							HISTOLOGY = True:
								MALAISE = True:
									Class die, prob=1.00
								MALAISE = False:
									SPIDERS = True:
										Class die, prob=1.00
									SPIDERS = False:
										SGOT = True:
											ANOREXIA = True:
												Class die, prob=1.00
											ANOREXIA = False:
												STEROID = True:
													Class die, prob=1.00
												STEROID = False:
													Class live, prob=1.00
										SGOT = False:
											Class die, prob=1.00
							HISTOLOGY = False:
								Class live, prob=1.00
					BILIRUBIN = False:
						Class die, prob=1.00
				BIGLIVER = False:
					Class live, prob=1.00


Reading test data from file part2/hepatitis-test.dat:

Number of categories: 2
categoryNames = [live, die]
Number of attributes: 16
attributeNames = [AGE, FEMALE, STEROID, ANTIVIRALS, FATIGUE, MALAISE, ANOREXIA, BIGLIVER, FIRMLIVER, SPLEENPALPABLE, SPIDERS, ASCITES, VARICES, BILIRUBIN, SGOT, HISTOLOGY]
Number of Instances: 27


Evaluating decision tree against test instances:

Test instance: live -> classified as: live
Test instance: die -> classified as: die
Test instance: live -> classified as: live
Test instance: live -> classified as: live
Test instance: live -> classified as: live
Test instance: live -> classified as: die
Test instance: live -> classified as: live
Test instance: live -> classified as: die
Test instance: live -> classified as: live
Test instance: live -> classified as: live
Test instance: live -> classified as: live
Test instance: live -> classified as: live
Test instance: die -> classified as: die
Test instance: live -> classified as: live
Test instance: live -> classified as: live
Test instance: die -> classified as: die
Test instance: live -> classified as: live
Test instance: live -> classified as: live
Test instance: live -> classified as: live
Test instance: live -> classified as: live
Test instance: live -> classified as: live
Test instance: live -> classified as: die
Test instance: live -> classified as: live
Test instance: live -> classified as: live
Test instance: live -> classified as: die
Test instance: die -> classified as: live
Test instance: live -> classified as: die
--------------------------------------
Success Rate: 77.77778%
