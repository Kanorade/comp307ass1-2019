package comp307.assignment1.part2;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DecisionTreeLearning {
	LearningSet trainingSet;
	LearningSet testSet;
	Node decisionTree;

	public DecisionTreeLearning(String trainingFileName, String testFileName) {
		// Store training data
		System.out.println("Reading training data from file " + trainingFileName + ":\n");
		trainingSet = new LearningSet(new File(trainingFileName));

		System.out.println("Number of categories: " + trainingSet.numCategories);
		System.out.println("categoryNames = " + trainingSet.categoryNames);
		System.out.println("Number of attributes: " + trainingSet.numAttributes);
		System.out.println("attributeNames = " + trainingSet.attributeNames);
		System.out.println("Number of Instances: " + trainingSet.allInstances.size());
		System.out.println();

		// build a decision tree from the data
		System.out.println("Generating decision tree:");
		Set<Instance> instances = new HashSet<>(trainingSet.allInstances);
		List<String> attributes = new ArrayList<>(trainingSet.attributeNames);
		decisionTree = buildTree(instances, attributes);

		// display tree generated
		decisionTree.report("\t");
		System.out.println();
		System.out.println();

		System.out.println("Reading test data from file " + testFileName + ":\n");
		testSet = new LearningSet(new File(testFileName));

		System.out.println("Number of categories: " + testSet.numCategories);
		System.out.println("categoryNames = " + testSet.categoryNames);
		System.out.println("Number of attributes: " + testSet.numAttributes);
		System.out.println("attributeNames = " + testSet.attributeNames);
		System.out.println("Number of Instances: " + testSet.allInstances.size());
		System.out.println();
		System.out.println();
	}

	public void evaluateTrainingData() {
		System.out.println("Evaluating decision tree against test instances:\n");
		int successCount = 0;
		for (Instance instance : testSet.allInstances) {
			boolean traversing = true;
			String testCategory = testSet.categoryNames.get(instance.getCategory());
			String categoryFound = "";
			Node node = decisionTree;
			while (traversing) {
				if (node.isLeaf()) {
					categoryFound = node.getCategory();
					traversing = false;
				}
				else {
					String nodesAtt = node.getAttribute();
					// attribute's state
					boolean state = instance.getAtt(testSet.attributeNames.indexOf(nodesAtt));
					if (state == true) {
						node = node.getTrueNode();
					} else {
						node = node.getFalseNode();
					}
				}
			}

			System.out.println("Test instance: " + testCategory +
					" -> classified as: " + categoryFound);
			if (testCategory.equals(categoryFound)) {
				successCount++;
			}
		}
		System.out.println("--------------------------------------");
		System.out.println("Success Rate: " + (((float)successCount/(float)testSet.allInstances.size() * 100) + "%"));
	}

	private Node buildTree(Set<Instance> instances, List<String> attributes) {
		if (instances.isEmpty()) {
			// return leaf containing the name and probability of the most probable
			// category across the whole dataset (i.e. the �baseline� predictor)
			Node leaf = getLeafFromInstances(trainingSet.allInstances);
			return leaf;
		}
		if (getPurity(instances) == 0.0f) {// if instances are pure (all in the same category)
			// return leaf containing the category of the instances in the node and
			// probability = 1
			String category = "";
			for (Instance ins: instances) {
				category = trainingSet.categoryNames.get(ins.getCategory());
			}
			Node leaf = new Node(1.0f, category);
			return leaf;
		}
		if (attributes.isEmpty()) {
			// return leaf containing the name and probability of the majority
			// class of the instances in the node (choose randomly if classes
			// are equal)
			Node leaf = getLeafFromInstances(instances);
			return leaf;
		}
		// otherwise find best attribute
		String bestAttribute = "";
		Set<Instance> bestTrueSet = new HashSet<>();
		Set<Instance> bestFalseSet = new HashSet<>();
		float bestPurity = 1.0f; 	// The lower, the better
		for (String att : attributes) {
			// Seperate instances into 2 sets
			Set<Instance> trueSet = new HashSet<>();
			Set<Instance> falseSet = new HashSet<>();

			for (Instance instance: instances) {
				int attributeIndex = trainingSet.attributeNames.indexOf(att);
				if (instance.getAtt(attributeIndex) == true) {	// "== true" is redundant but is more readable
					trueSet.add(instance);
				} else {
					falseSet.add(instance);
				}
			}

			// get average purity of both sets
			float truePurity = getPurity(trueSet);
			float falsePurity = getPurity(falseSet);
			float averagePurity = (truePurity + falsePurity) / 2;

			if (averagePurity < bestPurity) {
				bestAttribute = att;
				bestTrueSet = trueSet;
				bestFalseSet = falseSet;
				bestPurity = averagePurity;
			}

		}

		// Build subtrees using the remaining attributes
		List<String> newAttributes = new ArrayList<>(attributes);
		newAttributes.remove(bestAttribute);
		Node trueNode = buildTree(bestTrueSet, newAttributes);
		Node falseNode = buildTree(bestFalseSet, newAttributes);
		return new Node(bestAttribute, falseNode, trueNode);
	}

	private Node getLeafFromInstances(Set<Instance> instances) {
		// This assumes there are only 2 classes/categories
		int firstCatCount = 0;
		int secondCatCount = 0;

		for (Instance instance : instances) {
			if (instance.getCategory() == 0) {
				firstCatCount++;
			} else if (instance.getCategory() == 1) {
				secondCatCount++;
			}
		}

		if (firstCatCount > secondCatCount) {
			float prob = (float)firstCatCount / (float)instances.size();
			String cat = trainingSet.categoryNames.get(0);
			return new Node(prob, cat);
		}
		else {
			float prob = (float)secondCatCount / (float)instances.size();
			String cat = trainingSet.categoryNames.get(1);
			return new Node(prob, cat);
		}
	}

	private float getPurity(Set<Instance> instanceSet) {
		// This method assumes there are only 2 classes/categories
		int firstCatCount = 0;
		int secondCatCount = 0;

		for (Instance instance : instanceSet) {
			if (instance.getCategory() == 0) {
				firstCatCount++;
			} else if (instance.getCategory() == 1) {
				secondCatCount++;
			}
		}
		// (Im)purity = P(firstCategory) * P(secondCategory)
		float firstFloat = (float)firstCatCount;
		float secondFloat = (float)secondCatCount;
		float prob1 = firstFloat / (firstFloat + secondFloat);
		float prob2 = secondFloat / (firstFloat + secondFloat);

		return prob1*prob2;
	}

	public static void main(String[] args) {
		if (args.length == 2) {
			DecisionTreeLearning dtl = new DecisionTreeLearning(args[0], args[1]);
			dtl.evaluateTrainingData();
        } else {
            System.out.println("Require 2 arguments.");
            System.out.println("Usage: DecisionTreeLearning trainingfile.txt testfile.txt");
        }
	}

}
