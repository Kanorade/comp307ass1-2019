package comp307.assignment1.part2;

import java.util.List;

public class Instance {
	private int category;
	private List<Boolean> values;
	
	public Instance(int category, List<Boolean> values) {
		super();
		this.category = category;
		this.values = values;
	}
	
	public int getCategory(){
		return category;
	}

	public boolean getAtt(int index){
		return values.get(index);
	}

	@Override
	public String toString() {
		return "Instance [category=" + category + ", values=" + values + "]";
	}
	
	
}
