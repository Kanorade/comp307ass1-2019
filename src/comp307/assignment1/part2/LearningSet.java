package comp307.assignment1.part2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class LearningSet {
	int numCategories;
	int numAttributes;
	List<String> categoryNames;
	List<String> attributeNames;
	Set<Instance> allInstances;

	/**
	 * Constructor creates itself from data of a given file.
	 * @param file
	 */
	public LearningSet(File file) {
		numCategories = 0;
		numAttributes = 0;
		categoryNames = new ArrayList<>();
		attributeNames = new ArrayList<>();
		allInstances = new HashSet<>();
		buildSetFromDataFile(file);

	}

	private void buildSetFromDataFile(File file) {
		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
			String line;

			/* first Line, sort out categories */
			line = br.readLine();	// Lets just assume the formating is correctS;
			sortCategories(line);

			/* Second line, sort out attributes */
			line = br.readLine();
			sortAttributes(line);

			/* Third line onwards, sort out instances */
			sortInstances(br);

		} catch (FileNotFoundException e) {
			System.out.println("Couldn't find the file specified.");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Something went wrong while reading the files: IOException");
			System.exit(0);
		}
	}

	private void sortInstances(BufferedReader br) throws IOException {
		String line;
		while ((line = br.readLine()) != null) {
			String[] tokens = line.split("\\s+");
			int cat = categoryNames.indexOf(tokens[0]);
			List<Boolean> bools = new ArrayList<>();
			for (int i = 1; i < tokens.length; i++) {
				bools.add(Boolean.parseBoolean(tokens[i]));
			}
			allInstances.add(new Instance(cat, bools));
		}
	}

	private void sortAttributes(String line) {
		String[] attributes = line.split("\t");
		numAttributes = attributes.length;
		for (int i = 0; i < numAttributes; i++) {
			attributeNames.add(attributes[i]);
		}
	}

	private void sortCategories(String line) {
		String[] categories = line.split("\t");
		numCategories = categories.length;
		for (int i = 0; i < numCategories; i++) {
			categoryNames.add(categories[i]);
		}
	}
}
