package comp307.assignment1.part2;

public class Node {
	private String attribute;
	private Node falseNode;
	private Node trueNode;
	
	private boolean isLeaf;
	private float probability;
	private String category;
	
	public Node(String attribute, Node falseNode, Node trueNode) {
		super();
		this.attribute = attribute;
		this.falseNode = falseNode;
		this.trueNode = trueNode;
		
		this.isLeaf = false;
		this.probability = -1;
		this.category = null;
		
	}
	
	/**
	 * Constructor for making a leaf node
	 */
	public Node (float prob, String category) {
		this.attribute = null;
		this.falseNode = null;
		this.trueNode = null;
		
		this.isLeaf = true;
		this.probability = prob;
		this.category = category;
	}
	
	public void report(String indent) {
		if (isLeaf) {
			if (category == null) {
				System.out.format("%sUnknown\n", indent);
			} else {
				System.out.format("%sClass %s, prob=%.2f\n", 
						indent, category, probability);
			}
		} else {
			System.out.format("%s%s = True:\n", indent, attribute);
			trueNode.report(indent + "\t");
			System.out.format("%s%s = False:\n", indent, attribute);
			falseNode.report(indent + "\t");
		}
	}
	public String getAttribute() {
		return attribute;
	}
	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}
	public Node getFalseNode() {
		return falseNode;
	}
	public void setFalseNode(Node falseNode) {
		this.falseNode = falseNode;
	}
	public Node getTrueNode() {
		return trueNode;
	}
	public void setTrueNode(Node trueNode) {
		this.trueNode = trueNode;
	}
	public boolean isLeaf() {
		return isLeaf;
	}
	public void setLeaf(boolean isLeaf) {
		this.isLeaf = isLeaf;
	}
	public float getProbability() {
		return probability;
	}
	public void setProbability(float probability) {
		this.probability = probability;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	@Override
	public String toString() {
		return "Node [attribute=" + attribute + ", falseNode=" + falseNode + ", trueNode=" + trueNode + ", isLeaf="
				+ isLeaf + ", probability=" + probability + "]";
	}
	
	
}
